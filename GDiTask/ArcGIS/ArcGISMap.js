﻿var map;
require([
  "esri/map",
  "esri/layers/FeatureLayer",
  "esri/dijit/PopupTemplate",
  "esri/request",
  "esri/geometry/Point",
  "esri/graphic",
  "dojo/on",
  "dojo/_base/array",
  "dojo/domReady!"
], function (
  Map,
  FeatureLayer,
  PopupTemplate,
  esriRequest,
  Point,
  Graphic,
  on,
  array
) {

    var featureLayer;

    map = new Map("map", {
        basemap: "streets",
        center: [18, 43],
        zoom: 7

    });

    //hide the popup if its outside the map's extent
    map.on("mouse-drag", function (evt) {
        if (map.infoWindow.isShowing) {
            var loc = map.infoWindow.getSelectedFeature().geometry;
            if (!map.extent.contains(loc)) {
                map.infoWindow.hide();
            }
        }
    });

    //create a feature collection for the truck
    var featureCollection = {
        "layerDefinition": null,
        "featureSet": {
            "features": [],
            "geometryType": "esriGeometryPoint"
        }
    };
    featureCollection.layerDefinition = {
        "geometryType": "esriGeometryPoint",
        "objectIdField": "ObjectID",
        "drawingInfo": {
            "renderer": {
                "type": "simple",
                "symbol": {
                    "type": "esriPMS",
                    "url": "images/truck.png",
                    "contentType": "image/png",
                    "width": 30,
                    "height": 30
                }
            }
        },
        "fields": [{
            "name": "ObjectID",
            "alias": "ObjectID",
            "type": "esriFieldTypeOID"
        }, {
            "name": "description",
            "alias": "Description",
            "type": "esriFieldTypeString"
        }, {
            "name": "title",
            "alias": "Title",
            "type": "esriFieldTypeString"
        }]
    };

    //define a popup template
    var popupTemplate = new PopupTemplate({
        title: "{title}",
        description: "{description}"
    });

    //create a feature layer based on the feature collection
    featureLayer = new FeatureLayer(featureCollection, {
        id: 'truckLayer',
        infoTemplate: popupTemplate
    });

    //associate the features with the popup on click
    featureLayer.on("click", function (evt) {
        map.infoWindow.setFeatures([evt.graphic]);
    });

    map.on("layers-add-result", function (results) {
        requestData();
    });
    //add the feature layer that contains the truck info to the map
    map.addLayers([featureLayer]);

    function requestData() {
        var requestHandle = esriRequest({
            url: "http://localhost:3276/getList",
            callbackParamName: "jsoncallback"
        });
        requestHandle.then(requestSucceeded, requestFailed);
    }

    function requestSucceeded(response, io) {
        //loop through the items and add to the feature layer
        var features = [];
        array.forEach(response, function (item) {
            var attr = {};
            attr["description"] = item.longitude + " , " + item.latitude
            attr["title"] = item.vehiclename;

            var geometry = new Point(item.longitude, item.latitude);

            var graphic = new Graphic(geometry);
            graphic.setAttributes(attr);
            features.push(graphic);
        });

        featureLayer.applyEdits(features, null, null);
    }

    function requestFailed(error) {
        console.log('failed');
    }
});