﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GDiTask.Models;

namespace GDiTask.Controllers
{
    public class VehicleController : ApiController
    {
        // Uncomment out if database is not set
        // ->
        // Vehicle[] vehicles = new Vehicle[]
        //{
        //     new Vehicle { id = 1, latitude = 45.554962, longitude = 18.695514,  vehiclename = "Truck1" },
        //     new Vehicle { id = 2, latitude = 46.163938, longitude = 16.833475,  vehiclename = "Truck2" },
        //     new Vehicle { id = 3, latitude = 45.815011, longitude = 15.981919,  vehiclename = "Truck3" },
        //     new Vehicle { id = 4, latitude = 43.508133, longitude = 16.440193,  vehiclename = "Truck4" },
        //     new Vehicle { id = 5, latitude = 45.492897, longitude = 15.555268,  vehiclename = "Truck5" },
        //     new Vehicle { id = 6, latitude = 42.650661, longitude = 18.094423,  vehiclename = "Truck6" }
        //};
        // <-

        Vehicle[] vehicles;

        [Route("getList")]
        public IEnumerable<Vehicle> GetAllVehicles()
        {
            //Comment out if no database is set
            // ->
            using (var db = new PostgresDotNetContext())
            {
                vehicles = db.Vehicle.ToArray();

            }
            // <-
            return vehicles;
        }

        public Vehicle GetVehicleById(int id)
        {
            var vehicle = vehicles.FirstOrDefault((p) => p.id == id);
            if (vehicle == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return vehicle;
        }

        public IEnumerable<Vehicle> GetVehicleByName(string name)
        {
            return vehicles.Where(p => string.Equals(p.vehiclename, name,
                    StringComparison.OrdinalIgnoreCase));
        }
    }

}
