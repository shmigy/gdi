﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GDiTask.Models
{
    public class PostgresDotNetContext : DbContext
    {
        public PostgresDotNetContext() : base(nameOrConnectionString: "PostgresDotNetContext") { }
        public DbSet<Vehicle> Vehicle { get; set; }
    }
}