﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GDiTask.Models
{
    [Table("vehicle", Schema = "public")]
    public class Vehicle
    {
        public int id {get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
        public string vehiclename { get; set; }

    }
}