"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vehicleservice_1 = require("./vehicles/vehicleservice");
var Truck = (function () {
    function Truck(id, longitude, latitude, vehiclename) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.vehiclename = vehiclename;
    }
    return Truck;
}());
var AppComponent = (function () {
    function AppComponent(vehicleService) {
        this.vehicleService = vehicleService;
        this.vehicle = new Truck();
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.vehicleService.getVehicleMedium().then(function (vehicles) { return _this.vehicles = vehicles; });
    };
    AppComponent.prototype.showDialogToAdd = function () {
        this.newVehicle = true;
        this.vehicle = new Truck();
        this.displayDialog = true;
    };
    AppComponent.prototype.save = function () {
        if (this.newVehicle)
            this.vehicles.push(this.vehicle);
        else
            this.vehicles[this.findSelectedVehicleIndex()] = this.vehicle;
        this.vehicle = null;
        this.displayDialog = false;
    };
    AppComponent.prototype.delete = function () {
        this.vehicles.splice(this.findSelectedVehicleIndex(), 1);
        this.vehicle = null;
        this.displayDialog = false;
    };
    AppComponent.prototype.onRowSelect = function (event) {
        this.newVehicle = false;
        this.vehicle = this.cloneVehicle(event.data);
        this.displayDialog = true;
    };
    AppComponent.prototype.cloneVehicle = function (v) {
        var vehicle = new Truck();
        for (var prop in v) {
            vehicle[prop] = v[prop];
        }
        return vehicle;
    };
    AppComponent.prototype.findSelectedVehicleIndex = function () {
        return this.vehicles.indexOf(this.selectedCar);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: 'app.component.html',
        moduleId: module.id
    }),
    __metadata("design:paramtypes", [vehicleservice_1.VehicleService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map