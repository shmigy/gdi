import { Component } from '@angular/core';
import {Vehicle} from './vehicles/vehicle';
import {VehicleService} from './vehicles/vehicleservice';

class Truck implements Vehicle {
    constructor(public id?, public longitude?, public latitude?, public vehiclename?) { }
}

@Component({
    selector: 'my-app',
    templateUrl: 'app.component.html',
    moduleId: module.id
})

export class AppComponent {
    displayDialog: boolean;

    vehicle: Vehicle = new Truck();

    selectedCar: Vehicle;

    newVehicle: boolean;

    vehicles: Vehicle[];

    constructor(private vehicleService: VehicleService) { }

    ngOnInit() {
        this.vehicleService.getVehicleMedium().then(vehicles => this.vehicles = vehicles);
    }

    showDialogToAdd() {
        this.newVehicle = true;
        this.vehicle = new Truck();
        this.displayDialog = true;
    }

    save() {
        if (this.newVehicle)
            this.vehicles.push(this.vehicle);
        else
            this.vehicles[this.findSelectedVehicleIndex()] = this.vehicle;

        this.vehicle = null;
        this.displayDialog = false;
    }

    delete() {
        this.vehicles.splice(this.findSelectedVehicleIndex(), 1);
        this.vehicle = null;
        this.displayDialog = false;
    }

    onRowSelect(event) {
        this.newVehicle = false;
        this.vehicle = this.cloneVehicle(event.data);
        this.displayDialog = true;
    }

    cloneVehicle(v: Vehicle): Vehicle {
        let vehicle = new Truck();
        for (let prop in v) {
            vehicle[prop] = v[prop];
        }
        return vehicle;
    }

    findSelectedVehicleIndex(): number {
        return this.vehicles.indexOf(this.selectedCar);
    }
}
