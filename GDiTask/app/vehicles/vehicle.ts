﻿export interface Vehicle {
    id?;
    longitude?;
    latitude?;
    vehiclename?;
}