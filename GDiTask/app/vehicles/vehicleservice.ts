﻿import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Vehicle} from '../../app/vehicles/vehicle';

@Injectable()
export class VehicleService {

    constructor(private http: Http) { }

    getVehicleMedium() {
        return this.http.get('http://localhost:3276/getList')
            .toPromise()
            .then(res => <Vehicle[]>res.json())
            .then(data => { return data; });
    }
}