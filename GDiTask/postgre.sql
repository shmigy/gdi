CREATE TABLE public.vehicle
(
    id integer NOT NULL,
    latitude numeric(9, 6) NOT NULL,
    longitude numeric(9, 6) NOT NULL,
    vehiclename character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT firstkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.vehicle
    OWNER to postgres;



insert into vehicle values (1,45.554962,18.695514,"Truck1");
insert into vehicle values (2,46.16393,16.833475,'Truck2');
insert into vehicle values (3,45.81501,15.981919,'Truck3');
insert into vehicle values (4,43.508132,16.440193,'Truck4');
insert into vehicle values (5,45.492897,15.555268,'Truck5');
insert into vehicle values (6,42.650661,18.094424,'Truck6');