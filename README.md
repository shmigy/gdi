# README #
Hint: Ukoliko nemate postavljenu bazu, u VehicleController.cs zakomentirajte/otkomentirajte naznačeno.
SQL code za kreiranje tablice i podataka se nalazi u GDiTask/postgre.sql

**Baza : **  
Postgresql   
Napraviti tablicu Vehicle: Id, Longitude, Latitude, Name  

![Screenshot_1.png](https://bitbucket.org/repo/baqqjk9/images/148870195-Screenshot_1.png)

**MVC WEB API : **  
metoda getList  
dohvaća listu vozila iz Postgres baze (Entity framework model)  

**Angular2 page : **  
PrimeNG grid control  
prikazuje podatke iz tablice  

**Karta : **  
ESRI ArcGis for JavaScript,  prikaz koordinati na karti  
Zoom kad se klikne na koordinatu (bonus)